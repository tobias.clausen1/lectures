```python tags=["initialize"]
 
from matplotlib import pyplot

import numpy as np
from scipy.optimize import curve_fit
from scipy.integrate import quad

from common import draw_classic_axes, configure_plotting

configure_plotting()

```

# Summary & review

??? info "Lecture video"

    <iframe width="100%" height="315" src="https://www.youtube-nocookie.com/embed/NuQgJRQuJy8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
