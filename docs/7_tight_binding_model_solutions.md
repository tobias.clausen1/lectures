```python tags=["initialize"]
import matplotlib
from matplotlib import pyplot

import numpy as np

from common import draw_classic_axes, configure_plotting

configure_plotting()

pi = np.pi
```

# Solutions for lecture 7 exercises

## Warm up exercises
#### Question 1.

Check by yourself

#### Question 2.

$$
[ v_g ] = \frac{[E]}{[p]} = \frac{m}{s}
$$

#### Question 3.

$$
[m^*] = \frac{[p^2]}{[E]} = kg
$$

#### Question 4.

$$
m^* = m_e,
$$

where $m_e$ is the free electron mass. This is expected because the free electrons are not subject to a potential

#### Question 5.

The effective mass is given by the curvature of the dispersion. Therefore, if the dispersion has a constant curvature, the effective mass is the same for all $k-$values. This is the case for the purely parabolic dispersion $\epsilon(k)\propto^2$ of the free electron model

## Exercise 1: Lattice vibrations

#### Question 1.

The group velocity is given by 
$$
v_g(k)=\frac{\partial \omega(k)}{\partial k} = a \sqrt{\frac{\kappa}{m}}\cos(\frac{ka}{2}) \text{sign}(k),
$$
where $\text{sign}(k)$ represents the sign of $k$.
#### Question 2.

The density of states is
$$
g(\omega) = \frac{L}{\pi} \left|\frac{1}{v_g}\right| 
= \frac{L}{a \pi} \sqrt{\frac{m}{\kappa}}\frac{1}{\cos(ka/2)}
= \frac{L}{a \pi} \sqrt{\frac{m}{\kappa}}\frac{1}{\sqrt{1-\sin^2(ka/2)}}
=\frac{2L}{a \pi} \frac{1}{\sqrt{4\kappa / m - \omega^2}},
$$
where we substituted back the dispersion relation.

#### Question 3.


```python
pyplot.subplot(1,2,1)
k = np.linspace(-pi+0.01, pi-0.01, 300)
pyplot.plot(k[0:149], np.sin(k[0:149])/(np.sqrt(1-np.cos(k[0:149]))),'b');
pyplot.plot(k[150:300], np.sin(k[150:300])/(np.sqrt(1-np.cos(k[150:300]))),'b');
pyplot.xlabel(r'$ka$'); pyplot.ylabel('$v(k)$');
pyplot.xticks([-pi, 0, pi], [r'$-\pi$', 0, r'$\pi$']);
pyplot.yticks([-np.sqrt(2), 0, np.sqrt(2)], [r'$-a\sqrt{\frac{\kappa}{m}}$', 0, r'$a\sqrt{\frac{\kappa}{m}}$']);
pyplot.tight_layout();

pyplot.subplot(1,2,2)
w = np.linspace(0, 0.95, 300);
g = 1/np.sqrt(1-w**2);
pyplot.plot(w, g, 'b');
pyplot.xlabel(r'$\omega$'); pyplot.ylabel('$g(w)$');
pyplot.xticks([0, 1], [0, r'$2\sqrt{\frac{k}{m}}$']);
pyplot.yticks([0.5, 1], [0, r'$\frac{L}{\pi a}\sqrt{\frac{m}{\kappa}}$']);
pyplot.tight_layout();
```

#### Question 4.


Hint: The group velocity is given as $v = \frac{d\omega}{dk}$, draw a coordinate system **under** or **above** the dispersion graph with $k$ on the x-axis in which you draw $\frac{d\omega}{dk}$. Draw a coordinate system **next** to the dispersion with *$g(\omega)$ on the y-axis* in which you graph $\big(\frac{d\omega}{dk}\big)^{-1}$.

??? hint "Plots"

    ![](figures/dispersion_groupv_dos.svg)


## Exercise 2: Vibrational heat capacity of a 1D monatomic chain

#### Question 1.


For the energy we have: 
$$
U = \int \hbar \omega g(\omega) (n_{BE}(\hbar \omega) + \frac{1}{2})d\omega 
$$ 
with $g(\omega)$ the density of states calculated in exercise 1 and $n_{BE}(\hbar \omega) = \frac{1}{e^{\hbar\omega/k_BT}-1}$ the Bose-Einstein distribution.

#### Question 2.

For the heat capacity we have: 
$$
C = \frac{d U }{d T} = \int g(\omega) \hbar\omega \frac{d n_{BE}(\hbar \omega)}{d T}d\omega
$$

## Exercise 3: Next-nearest neighbors chain

#### Question 1.

The Schrödinger equation is $H|\Psi\rangle = E|\Psi\rangle$. The wavefunction is $|\Psi\rangle = \sum_m \phi_m |m\rangle$. By calculating $\langle n |H|\Psi\rangle$, we find 
$$ 
E\phi_n = E_0\phi_n - t\phi_{n-1} - t\phi_{n+1} - t'\phi_{n-2} - t'\phi_{n+2}
$$

#### Question 2.
We solve the previous equation using the Ansatz $\phi_n=e^{ikna}\phi_0$. Doing so, we find the dispersion relation:
$$
E(k) = E_0 - 2t\cos(ka) - 2t'\cos(2ka)
$$

#### Question 3.

The effective mass is 
$$
m^* = \frac{\hbar^2}{2a^2}\frac{1}{t\cos(ka)+4t'\cos(2ka)}
$$

Plot for $t=t'$:

```python
k1 = np.linspace(-pi, -pi/2-0.01, 300);
k2 = np.linspace(-pi/2+0.01, pi/2-0.01, 300);
k3 = np.linspace(pi/2+0.01, pi, 300);

pyplot.plot(k1, 1/(5*np.cos(k1)),'b');
pyplot.plot(k2, 1/(5*np.cos(k2)),'b');
pyplot.plot(k3, 1/(5*np.cos(k3)),'b');
pyplot.xlabel(r'$k$'); pyplot.ylabel('$m_{eff}(k)$');
pyplot.xticks([-pi,0,pi],[r'$-\pi/a$',0,r'$\pi/a$']);
pyplot.yticks([],[]);
pyplot.tight_layout();
```

#### Question 4.


Plots for $t=2t'$, $t=4t'$, and $t=10t'$:

```python
def m(k,t):
    return 1/(t*np.cos(k)+4*np.cos(2*k))
 
k1 = np.linspace(-pi, -pi/2-0.01, 300);
k2 = np.linspace(-pi/2+0.01, pi/2-0.01, 300);
k3 = np.linspace(pi/2+0.01, pi, 300);

pyplot.plot(k1, m(k1,2),'b');
pyplot.plot(k2, m(k2,2),'b');
pyplot.plot(k3, m(k3,2),'b',label='t=2t\'');
pyplot.xlabel('$k$'); pyplot.ylabel('$m_{eff}(k)$');
pyplot.xticks([-pi,0,pi],[r'$-\pi/a$',0,r'$\pi/a$']);
pyplot.yticks([0],[]);
pyplot.tight_layout();
pyplot.ylim(-3,3)
pyplot.plot(k1, m(k1,4),'r');
pyplot.plot(k2, m(k2,4),'r');
pyplot.plot(k3, m(k3,4),'r',label='t=4t\'');

pyplot.plot(k1, m(k1,10),'k');
pyplot.plot(k2, m(k2,10),'k');
pyplot.plot(k3, m(k3,10),'k',label='t=10t\'');

pyplot.legend();
```


