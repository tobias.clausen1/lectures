# Open Solid State Notes

!!! summary "Learning goals"

    After following this course you will be able to:

    - Model collective properties of fermions and bosons using density of states
    - Determine and analyse band structures of simple models of solid state systems
    - Describe and analyse properties of crystals
    - Analyse properties of semiconductor devices

In these notes our aim is to provide learning materials which are:

- self-contained
- easy to modify and remix, so we provide the full source, including the code
- open for reuse: see the license below.

The course is heavily inspired by the book [*Oxford Solid State Basics*](https://www-thphys.physics.ox.ac.uk/people/SteveSimon/book.html) by Steve Simon (referred to as *the book* in the materials).
We invite all the readers to read the book for a more detailed overview and [view his lecture recordings](https://podcasts.ox.ac.uk/series/oxford-solid-state-basics).

Whether you are a student taking this course, or an instructor reusing the materials, we welcome all contributions, so check out the [course repository](https://gitlab.kwant-project.org/solidstate/lectures), especially do [let us know](https://gitlab.kwant-project.org/solidstate/lectures/issues/new?issuable_template=typo) if you see a typo!
